//import { ref } from "vue";

export class Escala{
    id = 0;
    exigencia = 0;
    puntajeMaximo = 0;
    notaMinima = 0;
    notaMaxima = 0;
}

export class Evaluacion{
    id = 0;
    descripcion = "";
    fecha = "";
    nombre = "";
    escala = new Escala;
}

export class Planificacion{
    id = 0;
    descripcion = "";
    evaluaciones = [];
    fechaInicio = "";
    fechaTermino = "";
}

export class Cronograma{
    descripcion = "Elija una asignatura y curso para ver su cronograma";
    planificaciones = [new Planificacion];
    fechaInicio = "";
    fechaTermino = "";
}

export class Asingatura{
    nombre  = "";
}


export class Curso{
    codigo = "";
    asignaturas = [];
}